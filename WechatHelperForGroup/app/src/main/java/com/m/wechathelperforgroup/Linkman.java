package com.m.wechathelperforgroup;

import java.io.Serializable;

/**
 * Created by m on 2018/7/16.
 */

public class Linkman implements Serializable {
    private String wechatId;
    private String name;

    public String getWechatId() {
        return wechatId;
    }

    public void setWechatId(String wechatId) {
        this.wechatId = wechatId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
